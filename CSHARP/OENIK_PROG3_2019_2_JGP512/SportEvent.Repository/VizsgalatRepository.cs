﻿// <copyright file="VizsgalatRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SportEvent.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using SportEvent.Data;

    /// <summary>
    /// sads.
    /// </summary>
    public class VizsgalatRepository : IRepository<Vizsgalat>
    {
        /// <summary>
        /// Get all vizsgalat as queryable.
        /// </summary>
        /// <returns>IQueryable.</returns>
        public IQueryable<Vizsgalat> GetAll()
        {
            return DbConnect.Peldany.GetAllVizsgalat().AsQueryable<Vizsgalat>();
        }

        /// <summary>
        /// Get all information from Vizsgalat.
        /// </summary>
        /// <returns>stringbuilder.</returns>
        public StringBuilder GetTableContents()
        {
            List<Vizsgalat> a = this.GetAll().ToList();
            StringBuilder s = new StringBuilder();
            foreach (var item in a)
            {
                s.AppendLine("ID:" + item.Vizsg_ID + " Versenyzok:" + item.Versenyzo.Nev + " Viszgálat eredmények:" + item.Eredmenye + " Dátuma:" + item.Datuma.ToShortDateString()
                    + " Orvosa:" + item.Orvos.Nev);
            }

            return s;
        }

        /// <summary>
        /// Insert method for Vizsgalat.
        /// </summary>
        /// <param name="elem">a.</param>
        public void Insert(List<string> elem)
        {
            Vizsgalat inVizsgalat = new Vizsgalat();
            inVizsgalat.Vizsg_ID = int.Parse(elem[0]);
            inVizsgalat.V_ID = int.Parse(elem[1]);
            inVizsgalat.Orvos_ID = int.Parse(elem[2]);
            inVizsgalat.Eredmenye = elem[3];
            inVizsgalat.Datuma = DateTime.Parse(elem[4]);
            DbConnect.Peldany.AddNewVizsgalat(inVizsgalat);
            DbConnect.Peldany.SaveDB();
        }

        /// <summary>
        /// Help Update method for vizsgalat.
        /// </summary>
        /// <param name="id">a-.</param>
        /// <param name="update">asd.</param>
        public void Update2(int id, Vizsgalat update)
        {
            Vizsgalat vizsgalat = DbConnect.Peldany.GetOneVizsgalat(id);
            vizsgalat.Vizsg_ID = update.Vizsg_ID;
            vizsgalat.V_ID = update.V_ID;
            vizsgalat.Orvos_ID = update.Orvos_ID;
            vizsgalat.Eredmenye = update.Eredmenye;
            vizsgalat.Datuma = update.Datuma;
            DbConnect.Peldany.SaveDB();
        }

        /// <summary>
        /// Help update method for vizsgalat.
        /// </summary>
        /// <param name="regi">asd.</param>
        /// <param name="elementoftable">er.</param>
        /// <returns>Viszgalat.</returns>
        public Vizsgalat VizsgalatUpdate(Vizsgalat regi, List<string> elementoftable)
        {
            Vizsgalat newVizsgalat = new Vizsgalat();
            if (elementoftable[0] == string.Empty)
            {
                newVizsgalat.Vizsg_ID = regi.Vizsg_ID;
            }
            else
            {
                newVizsgalat.Vizsg_ID = int.Parse(elementoftable[0]);
            }

            if (elementoftable[1] == string.Empty)
            {
                newVizsgalat.V_ID = regi.V_ID;
            }
            else
            {
                newVizsgalat.V_ID = int.Parse(elementoftable[1]);
            }

            if (elementoftable[2] == string.Empty)
            {
                newVizsgalat.Orvos_ID = regi.Orvos_ID;
            }
            else
            {
                newVizsgalat.Orvos_ID = int.Parse(elementoftable[2]);
            }

            if (elementoftable[3] == string.Empty)
            {
                newVizsgalat.Eredmenye = regi.Eredmenye;
            }
            else
            {
                newVizsgalat.Eredmenye = elementoftable[3];
            }

            if (elementoftable[4] == string.Empty)
            {
                newVizsgalat.Datuma = regi.Datuma;
            }
            else
            {
                newVizsgalat.Datuma = DateTime.Parse(elementoftable[4]);
            }

            return newVizsgalat;
        }

        /// <summary>
        /// final update method.
        /// </summary>
        /// <param name="id">d.</param>
        /// <param name="elementoftable">e.</param>
        /// <returns>t/f.</returns>
        public bool Update(int id, List<string> elementoftable)
        {
            Vizsgalat vizsgalat = DbConnect.Peldany.GetOneVizsgalat(id);
            if (vizsgalat != null)
            {
                this.Update2(id, this.VizsgalatUpdate(vizsgalat, elementoftable));
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// delet method for Vizsgalat.
        /// </summary>
        /// <param name="id">a.</param>
        /// <returns>t/f.</returns>
        public bool Delete2(int id)
        {
            if (DbConnect.Peldany.GetOneVizsgalat(id) != null)
            {
               if (DbConnect.Peldany.DeleteVizsgalat2(id))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
