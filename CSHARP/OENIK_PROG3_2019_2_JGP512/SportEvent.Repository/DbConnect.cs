﻿// <copyright file="DbConnect.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SportEvent.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using SportEvent.Data;

    /// <summary>
    /// Make the connection between the DB and repo classes.
    /// </summary>
    internal class DbConnect /*: IDisposable*/
    {
        private static DbConnect peldany;
        private readonly DatabaseEntities databaseEntities;

        /// <summary>
        /// Initializes a new instance of the <see cref="DbConnect"/> class.
        /// Db ctor.
        /// </summary>
        public DbConnect()
        {
            this.databaseEntities = new DatabaseEntities();
        }

        /// <summary>
        /// Gets.
        /// </summary>
        public static DbConnect Peldany
        {
            get
            {
                if (peldany == null)
                {
                    peldany = new DbConnect();
                }

                return peldany;
            }
        }

        //---------------------------------------------------------------Insert--------------------------------------------------------------------------------

        /// <summary>
        /// Add new versenyzo(Insert).
        /// </summary>
        /// <param name="versenyzo">v.</param>
        public virtual void AddNewVersenyzo(Versenyzo versenyzo)
        {
            this.databaseEntities.Versenyzo.Add(versenyzo);
        }

        /// <summary>
        /// Add new edzo(Insert).
        /// </summary>
        /// <param name="edzo">e.</param>
        public void AddNewEdzo(Edzo edzo)
        {
            this.databaseEntities.Edzo.Add(edzo);
        }

        /// <summary>
        /// Add new Orvos(insert).
        /// </summary>
        /// <param name="orvos">o.</param>
        public void AddNewOrvos(Orvos orvos)
        {
            this.databaseEntities.Orvos.Add(orvos);
        }

        /// <summary>
        /// Add new Vizsgalat(insert).
        /// </summary>
        /// <param name="vizsgalatok">as.</param>
        public virtual void AddNewVizsgalat(Vizsgalat vizsgalatok)
        {
            this.databaseEntities.Vizsgalat.Add(vizsgalatok);
        }

        //-------------------------------------------------------Delet----------------------------------------------------------------------------------------

          /// <summary>
          /// Delete versenyzo.
          /// </summary>
          /// <param name="id">a.</param>
          /// <returns>h.</returns>
        public bool DeleteVersenyzo2(int id)
        {
            try
            {
                Versenyzo toDelete = this.databaseEntities.Versenyzo.Single(x => x.V_ID == id);
                this.databaseEntities.Versenyzo.Remove(toDelete);
                this.SaveDB();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

       /// <summary>
       /// Delet method for Edzo.
       /// </summary>
       /// <param name="id">a.</param>
       /// <returns>aa.</returns>
        public bool DeleteEdzo2(int id)
        {
            try
            {
            Edzo toDelete = this.databaseEntities.Edzo.Single(x => x.Edzo_ID == id);
            this.databaseEntities.Edzo.Remove(toDelete);
            this.SaveDB();
            return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Delet method for orvos.
        /// </summary>
        /// <param name="id">a.</param>
        /// <returns>aa.</returns>
        public bool DeleteOrvos2(int id)
        {
            try
            {
                Orvos toDelete = this.databaseEntities.Orvos.Single(x => x.Orvos_ID == id);
                this.databaseEntities.Orvos.Remove(toDelete);
                this.SaveDB();
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delet method for vizsgalat.
        /// </summary>
        /// <param name="id">a.</param>
        /// <returns>aa.</returns>
        public bool DeleteVizsgalat2(int id)
        {
            try
            {
                Vizsgalat toDelete = this.databaseEntities.Vizsgalat.Single(x => x.Vizsg_ID == id);
                this.databaseEntities.Vizsgalat.Remove(toDelete);
                this.SaveDB();
                return true;
            }
            catch
            {
                return false;
            }
        }

// --------------------------------------------------------------------------------------------------------------------------------------

        /// <summary>
        /// Get the table names.
        /// </summary>
        /// <returns>list of string.</returns>
        public List<string> GetTableNames()
        {
            List<string> tables = new List<string> { "Edzo", "Versenyzo", "Orvos", "Vizsgalat" };
            return tables;
        }

        /// <summary>
        /// Read all Versenyzo.
        /// </summary>
        /// <returns>List of versenyzo.</returns>
        public virtual List<Versenyzo> GetAllVersenyzo()
        {
            var allVersenyzok = this.databaseEntities.Versenyzo.Select(x => x);
            return allVersenyzok.ToList();
        }

        /// <summary>
        /// Read all edzo from db.
        /// </summary>
        /// <returns>List fo edzo.</returns>
        public virtual List<Edzo> GetAllEdzo()
        {
            var allEdzok = this.databaseEntities.Edzo.Select(x => x);
            return allEdzok.ToList();
        }

        /// <summary>
        /// Read all Orvos from db.
        /// </summary>
        /// <returns>list of Orvos.</returns>
        public virtual List<Orvos> GetAllOrvos()
        {
            var allOrvos = this.databaseEntities.Orvos.Select(x => x);
            return allOrvos.ToList();
        }

        /// <summary>
        /// Read all Vizsgalat from db.
        /// </summary>
        /// <returns>List of Vizsgalat.</returns>
        public virtual List<Vizsgalat> GetAllVizsgalat()
        {
            var allVizsgalat = this.databaseEntities.Vizsgalat.Select(x => x);
            return allVizsgalat.ToList();
        }

        // ---------------------------------------------------------------------------------

        /// <summary>
        /// Return one Versenyzo.
        /// </summary>
        /// <param name="id">n.</param>
        /// <returns>Versenyzo.</returns>
        public Versenyzo GetOneVersenyzo(int id)
        {
            try
            {
                Versenyzo ver = this.databaseEntities.Versenyzo.Single(x => x.V_ID == id);
                return ver;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Get one Edzo.
        /// </summary>
        /// <param name="id">n.</param>
        /// <returns>Edzo.</returns>
        public Edzo GetOneEdzo(int id)
        {
            try
            {
                Edzo ver = this.databaseEntities.Edzo.Single(x => x.Edzo_ID == id);
                return ver;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Get one Orvos.
        /// </summary>
        /// <param name="id">n.</param>
        /// <returns>Orvos.</returns>
        public Orvos GetOneOrvos(int id)
        {
            try
            {
                Orvos ver = this.databaseEntities.Orvos.Single(x => x.Orvos_ID == id);
                return ver;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Get one Vizsglat.
        /// </summary>
        /// <param name="id">n.</param>
        /// <returns>Vizsgalat.</returns>
        public Vizsgalat GetOneVizsgalat(int id)
        {
            try
            {
                Vizsgalat ver = this.databaseEntities.Vizsgalat.Single(x => x.Vizsg_ID == id);
                return ver;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Save all changes in db.
        /// </summary>
        public virtual void SaveDB()
        {
            this.databaseEntities.SaveChanges();
        }
    }
}
