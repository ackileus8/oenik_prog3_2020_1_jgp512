﻿// <copyright file="OrvosRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SportEvent.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using SportEvent.Data;

    /// <summary>
    /// asda.
    /// </summary>
    public class OrvosRepository : IRepository<Orvos>
    {
        /// <summary>
        /// Return all orvos AsQueryable.
        /// </summary>
        /// <returns>IQueryable.</returns>
        public IQueryable<Orvos> GetAll()
        {
            return DbConnect.Peldany.GetAllOrvos().AsQueryable<Orvos>();
        }

        /// <summary>
        /// Get all orvos.
        /// </summary>
        /// <returns>stringbuilder.</returns>
        public StringBuilder GetTableContents()
        {
            List<Orvos> a = this.GetAll().ToList();
            StringBuilder s = new StringBuilder();
            foreach (var item in a)
            {
                s.AppendLine("ID:" + item.Orvos_ID + " Név:" + item.Nev + " Sportja:" + item.Sportja + " Orvosi szakirány:" + item.Szakirany
                    + " Vizsgált már beteget?: " + item.VizsgaltEMar);
            }

            return s;
        }

            /// <summary>
            /// Update Orvos db.
            /// </summary>
            /// <param name="id">n.</param>
            /// <param name="update">na.</param>
        public void Update(int id, Orvos update)
        {
            Orvos regiOrvos = DbConnect.Peldany.GetOneOrvos(id);
            regiOrvos.Orvos_ID = update.Orvos_ID;
            regiOrvos.Nev = update.Nev;
            regiOrvos.Szakirany = update.Szakirany;
            regiOrvos.Elerhetoseg = update.Elerhetoseg;
            regiOrvos.Sportja = update.Sportja;
            regiOrvos.VizsgaltEMar = update.VizsgaltEMar;
            DbConnect.Peldany.SaveDB();
        }

        /// <summary>
        /// Help Orvos update method.
        /// </summary>
        /// <param name="regi">a.</param>
        /// <param name="elementoftable">asad.</param>
        /// <returns>Orvos.</returns>
        public Orvos OrvosUpdate(Orvos regi, List<string> elementoftable)
        {
            Orvos newOrvos = new Orvos();
            if (elementoftable[0] == string.Empty)
            {
                newOrvos.Orvos_ID = regi.Orvos_ID;
            }
            else
            {
                newOrvos.Orvos_ID = int.Parse(elementoftable[0]);
            }

            if (elementoftable[1] == string.Empty)
            {
                newOrvos.Nev = regi.Nev;
            }
            else
            {
                newOrvos.Nev = elementoftable[1];
            }

            if (elementoftable[2] == string.Empty)
            {
                newOrvos.Szakirany = regi.Szakirany;
            }
            else
            {
                newOrvos.Szakirany = elementoftable[2];
            }

            if (elementoftable[3] == string.Empty)
            {
                newOrvos.Elerhetoseg = regi.Elerhetoseg;
            }
            else
            {
                newOrvos.Elerhetoseg = elementoftable[3];
            }

            if (elementoftable[4] == string.Empty)
            {
                newOrvos.Sportja = regi.Sportja;
            }
            else
            {
                newOrvos.Sportja = elementoftable[4];
            }

            if (elementoftable[5] == string.Empty)
            {
                newOrvos.VizsgaltEMar = regi.VizsgaltEMar;
            }
            else
            {
                newOrvos.VizsgaltEMar = elementoftable[5];
            }

            return newOrvos;
        }

        /// <summary>
        /// Final update method for orvos.
        /// </summary>
        /// <param name="id">a.</param>
        /// <param name="elementoftable">e.</param>
        /// <returns>t/f.</returns>
        public bool Update(int id, List<string> elementoftable)
        {
            Orvos regiOrvos = DbConnect.Peldany.GetOneOrvos(id);
            if (regiOrvos != null)
            {
                this.Update(id, this.OrvosUpdate(regiOrvos, elementoftable));
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Insert method for orvos.
        /// </summary>
        /// <param name="elem">n.</param>
        public void Insert(List<string> elem)
        {
            Orvos inOrvos = new Orvos();
            inOrvos.Orvos_ID = int.Parse(elem[0]);
            inOrvos.Nev = elem[1];
            inOrvos.Szakirany = elem[2];
            inOrvos.Elerhetoseg = elem[4];
            inOrvos.Sportja = elem[5];
            inOrvos.VizsgaltEMar = elem[5];
            DbConnect.Peldany.AddNewOrvos(inOrvos);
            DbConnect.Peldany.SaveDB();
        }

        /// <summary>
        /// 2. Nem CRUD művelet.
        /// </summary>
        /// <returns>Visszaadja azokat az orvosokat akik megtagadtak egy versenyzőtől a  sportorvosi igazolását.</returns>
        public IQueryable OrvosListazasaMegtagadottVizsgalatiEredmenyAlapjan()
        {
            VizsgalatRepository repoJUmp = new VizsgalatRepository();
            IQueryable<Orvos> orvosok = this.GetAll();
            IQueryable<Vizsgalat> vizsgalatok = repoJUmp.GetAll();

            var q = orvosok.Join(
                vizsgalatok,
                orvos => orvos.Orvos_ID,
                vizsgalat => vizsgalat.Orvos_ID,
                (orvos, vizsgalat) => new
                {
                    OrvoNev = orvos.Nev,
                    vizsgalat = vizsgalat.Eredmenye,
                    vizsgalat.Datuma,
                }).Where(x => x.vizsgalat == "Megtagadva").OrderBy(y => y.OrvoNev).AsQueryable();
            return q;
           }

        /// <summary>
        /// Delete method for Orvos.
        /// </summary>
        /// <param name="id">a.</param>
        /// <returns>t/f.</returns>
        public bool Delete2(int id)
        {
            if (DbConnect.Peldany.GetOneOrvos(id) != null)
            {
               if (DbConnect.Peldany.DeleteOrvos2(id))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}
