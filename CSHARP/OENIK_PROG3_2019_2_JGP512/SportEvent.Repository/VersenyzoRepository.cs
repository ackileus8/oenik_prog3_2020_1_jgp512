﻿// <copyright file="VersenyzoRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SportEvent.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using SportEvent.Data;

    /// <summary>
    /// sad.
    /// </summary>
    public class VersenyzoRepository : IRepository<Versenyzo>
    {
        /// <summary>
        /// get all versenyzo as Queryable .
        /// </summary>
        /// <returns>IQueryable.</returns>
        public IQueryable<Versenyzo> GetAll()
        {
            return DbConnect.Peldany.GetAllVersenyzo().AsQueryable<Versenyzo>();
        }

        /// <summary>
        /// Return all modells,.
        /// </summary>
        /// <returns>IQueryable values.</returns>
        public StringBuilder GetTableContents()
        {
           List<Versenyzo> a = this.GetAll().ToList();
           StringBuilder s = new StringBuilder();
           foreach (var item in a)
           {
             s.AppendLine("ID:" + item.V_ID + " Név:" + item.Nev + " Neme:" + item.Nem + " Súlya" + item.Suly + " Kora" + item.Kor + " klubja" +
                 item.Klub + " övfokozata" + item.Ovfokozat + " küzdelem" + item.Küzdelem + " forma" + item.Formagy + " edző" + item.Edzo.Nev);
           }

           return s;
        }

     /// <summary>
     /// Update method for Versenyzo.
     /// </summary>
     /// <param name="id">n.</param>
     /// <param name="update">u.</param>
        public void Update(int id, Versenyzo update)
        {
            Versenyzo regiversenyzo = DbConnect.Peldany.GetOneVersenyzo(id);
            regiversenyzo.V_ID = update.V_ID;
            regiversenyzo.Nev = update.Nev;
            regiversenyzo.Nem = update.Nem;
            regiversenyzo.Suly = update.Suly;
            regiversenyzo.Kor = update.Kor;
            regiversenyzo.Klub = update.Klub;
            regiversenyzo.Ovfokozat = update.Ovfokozat;
            regiversenyzo.Küzdelem = update.Küzdelem;
            regiversenyzo.Formagy = update.Formagy;
            regiversenyzo.Edzo_ID = update.Edzo_ID;
            DbConnect.Peldany.SaveDB();
        }

        /// <summary>
        /// Help to make the update for versenyzo.
        /// </summary>
        /// <param name="regi">n.</param>
        /// <param name="elementoftable">nf.</param>
        /// <returns>Return new versenyzo.</returns>
        public Versenyzo VersenyzoUpdate(Versenyzo regi, List<string> elementoftable)
        {
            Versenyzo newVersenyzo = new Versenyzo();
            if (elementoftable[0] == string.Empty)
            {
                newVersenyzo.V_ID = regi.V_ID;
            }
            else
            {
                newVersenyzo.V_ID = int.Parse(elementoftable[0]);
            }

            if (elementoftable[1] == string.Empty)
            {
                newVersenyzo.Nev = regi.Nev;
            }
            else
            {
                newVersenyzo.Nev = elementoftable[1];
            }

            if (elementoftable[2] == string.Empty)
            {
                newVersenyzo.Nem = regi.Nem;
            }
            else
            {
                newVersenyzo.Nem = elementoftable[2];
            }

            if (elementoftable[3] == string.Empty)
            {
                newVersenyzo.Suly = regi.Suly;
            }
            else
            {
                newVersenyzo.Suly = int.Parse(elementoftable[3]);
            }

            if (elementoftable[4] == string.Empty)
            {
                newVersenyzo.Kor = regi.Kor;
            }
            else
            {
                newVersenyzo.Kor = int.Parse(elementoftable[4]);
            }

            if (elementoftable[5] == string.Empty)
            {
                newVersenyzo.Klub = regi.Klub;
            }
            else
            {
                newVersenyzo.Klub = elementoftable[5];
            }

            if (elementoftable[6] == string.Empty)
            {
                newVersenyzo.Ovfokozat = regi.Ovfokozat;
            }
            else
            {
                newVersenyzo.Ovfokozat = elementoftable[6];
            }

            if (elementoftable[7] == string.Empty)
            {
                newVersenyzo.Küzdelem = regi.Küzdelem;
            }
            else
            {
                newVersenyzo.Küzdelem = elementoftable[7];
            }

            if (elementoftable[8] == string.Empty)
            {
                newVersenyzo.Formagy = regi.Formagy;
            }
            else
            {
                newVersenyzo.Formagy = elementoftable[8];
            }

            if (elementoftable[9] == string.Empty)
            {
                newVersenyzo.Edzo_ID = regi.Edzo_ID;
            }
            else
            {
                newVersenyzo.Edzo_ID = int.Parse(elementoftable[9]);
            }

            return newVersenyzo;
        }

        /// <summary>
        /// Final update method.
        /// </summary>
        /// <param name="id">a.</param>
        /// <param name="elementoftable">f.</param>
        /// <returns>t/f.</returns>
        public bool Update(int id, List<string> elementoftable)
        {
            Versenyzo regiVersenyzo = DbConnect.Peldany.GetOneVersenyzo(id);
            if (regiVersenyzo != null)
            {
                this.Update(id, this.VersenyzoUpdate(regiVersenyzo, elementoftable));
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Insert method for versenyzo.
        /// </summary>
        /// <param name="elem">a.</param>
        public void Insert(List<string> elem)
        {
            Versenyzo inVersenyzo = new Versenyzo();
            inVersenyzo.V_ID = int.Parse(elem[0]);
            inVersenyzo.Nev = elem[1];
            inVersenyzo.Nem = elem[2];
            inVersenyzo.Suly = int.Parse(elem[3]);
            inVersenyzo.Kor = int.Parse(elem[4]);
            inVersenyzo.Klub = elem[5];
            inVersenyzo.Ovfokozat = elem[6];
            inVersenyzo.Küzdelem = elem[7];
            inVersenyzo.Formagy = elem[8];
            inVersenyzo.Edzo_ID = int.Parse(elem[9]);
            DbConnect.Peldany.AddNewVersenyzo(inVersenyzo);
            DbConnect.Peldany.SaveDB();
        }

        /// <summary>
        /// delete method for versenyzo.
        /// </summary>
        /// <param name="id">a.</param>
        /// <returns>t/f.</returns>
        public bool Delete2(int id)
        {
            if (DbConnect.Peldany.GetOneVersenyzo(id) != null)
            {
                if (DbConnect.Peldany.DeleteVersenyzo2(id))
                {
                   return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Non CRUD method.
        /// </summary>
        /// <returns>IQueryable.<returns>
        public IQueryable IndulhatVersenyzoAVersenyen()
        {
            VizsgalatRepository repoJUmp = new VizsgalatRepository();
            IQueryable<Versenyzo> versenyzok = this.GetAll();
            IQueryable<Vizsgalat> vizsgalatok = repoJUmp.GetAll();

            var q = versenyzok.Join(
                vizsgalatok,
                versenyzo => versenyzo.V_ID,
                vizsgalat => vizsgalat.V_ID,
                (versenyzo, vizsgalat) => new
                {
                    VersenyzoNeve = versenyzo.Nev,
                    versenyzo.Klub,
                    versenyzo.Küzdelem,
                    versenyzo.Formagy,
                    vizsgalat.Eredmenye,
                }).Where(x => x.Eredmenye != "Megtagadva").OrderBy(y => y.VersenyzoNeve).AsQueryable();
            return q;
        }
 }
}
