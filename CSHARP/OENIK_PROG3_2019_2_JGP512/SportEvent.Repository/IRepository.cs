﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SportEvent.Repository
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Interface for Repo.
    /// </summary>
    /// <typeparam name="T">type of repos.</typeparam>
    public interface IRepository<T>
    {
        /// <summary>
        /// Contents as stinrg.
        /// </summary>
        /// <returns>Stringbuilder.</returns>
        StringBuilder GetTableContents();

        /// <summary>
        /// Return all the elements in tabels.
        /// </summary>
        /// <returns>Query type.</returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Insert all.
        /// </summary>
        /// <param name="elem">n.</param>
        void Insert(List<string> elem);

        /// <summary>
        /// Delet all.
        /// </summary>
        /// <param name="id">n.</param>
        /// <returns>id.</returns>
        bool Delete2(int id);

        /// <summary>
        /// Update interface method.
        /// </summary>
        /// <param name="id">n.</param>
        /// <param name="elementoftable">m.</param>
        /// <returns>T/F.</returns>
        bool Update(int id, List<string> elementoftable);
    }
}
