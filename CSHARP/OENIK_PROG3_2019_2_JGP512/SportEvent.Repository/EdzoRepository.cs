﻿// <copyright file="EdzoRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SportEvent.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using SportEvent.Data;

    /// <summary>
    /// asd.
    /// </summary>
    public class EdzoRepository : IRepository<Edzo>
    {
        /// <summary>
        /// Return all edzo.
        /// </summary>
        /// <returns>IQueryable.</returns>
        public IQueryable<Edzo> GetAll()
        {
            return DbConnect.Peldany.GetAllEdzo().AsQueryable<Edzo>();
        }

        /// <summary>
        /// Get the Edzo table.
        /// </summary>
        /// <returns>a.</returns>
        public StringBuilder GetTableContents()
        {
            List<Edzo> a = this.GetAll().ToList();
            StringBuilder s = new StringBuilder();
            foreach (var item in a)
            {
                s.AppendLine("ID:" + item.Edzo_ID + " Nev:" + item.Nev + " Klub:" + item.Klub +
                 " Elérhetősége:" + item.Elerhetoseg + " Ovfokozata:" + item.Ovfokozat +
                  " Kora:" + item.Kor);
            }

            return s;
        }

        /// <summary>
        /// Insert edzo.
        /// </summary>
        /// <param name="elem">a.</param>
        public void Insert(List<string> elem)
        {
            Edzo inEdzo = new Edzo();
            inEdzo.Edzo_ID = int.Parse(elem[0]);
            inEdzo.Nev = elem[1];
            inEdzo.Klub = elem[2];
            inEdzo.Elerhetoseg = elem[3];
            inEdzo.Ovfokozat = elem[4];
            inEdzo.Kor = int.Parse(elem[5]);
            DbConnect.Peldany.AddNewEdzo(inEdzo);
            DbConnect.Peldany.SaveDB();
        }

        /// <summary>
        /// delete method for edzo.
        /// </summary>
        /// <param name="id">a.</param>
        /// <returns>t/f.</returns>
        public bool Delete2(int id)
        {
            if (DbConnect.Peldany.GetOneEdzo(id) != null)
            {
                if (DbConnect.Peldany.DeleteEdzo2(id))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// update method for orvos.
        /// </summary>
        /// <param name="id">a.</param>
        /// <param name="update">e.</param>
        public void Update(int id, Edzo update)
        {
            Edzo regiOrvos = DbConnect.Peldany.GetOneEdzo(id);
            regiOrvos.Edzo_ID = update.Edzo_ID;
            regiOrvos.Nev = update.Nev;
            regiOrvos.Klub = update.Klub;
            regiOrvos.Elerhetoseg = update.Elerhetoseg;
            regiOrvos.Ovfokozat = update.Ovfokozat;
            regiOrvos.Kor = update.Kor;
            DbConnect.Peldany.SaveDB();
        }

        /// <summary>
        /// Help Update method for orvos.
        /// </summary>
        /// <param name="regi">a.</param>
        /// <param name="elementoftable">e.</param>
        /// <returns>ak.</returns>
        public Edzo OrvosUpdate(Edzo regi, List<string> elementoftable)
        {
            Edzo newEdzo = new Edzo();
            if (elementoftable[0] == string.Empty)
            {
                newEdzo.Edzo_ID = regi.Edzo_ID;
            }
            else
            {
                newEdzo.Edzo_ID = int.Parse(elementoftable[0]);
            }

            if (elementoftable[1] == string.Empty)
            {
                newEdzo.Nev = regi.Nev;
            }
            else
            {
                newEdzo.Nev = elementoftable[1];
            }

            if (elementoftable[2] == string.Empty)
            {
                newEdzo.Klub = regi.Klub;
            }
            else
            {
                newEdzo.Klub = elementoftable[2];
            }

            if (elementoftable[3] == string.Empty)
            {
                newEdzo.Elerhetoseg = regi.Elerhetoseg;
            }
            else
            {
                newEdzo.Elerhetoseg = elementoftable[3];
            }

            if (elementoftable[4] == string.Empty)
            {
                newEdzo.Ovfokozat = regi.Ovfokozat;
            }
            else
            {
                newEdzo.Ovfokozat = elementoftable[4];
            }

            if (elementoftable[5] == string.Empty)
            {
                newEdzo.Kor = regi.Kor;
            }
            else
            {
                newEdzo.Kor = int.Parse(elementoftable[5]);
            }

            return newEdzo;
        }

        /// <summary>
        /// Update method for edzo.
        /// </summary>
        /// <param name="id">d.</param>
        /// <param name="elementoftable">as.</param>
        /// <returns>t/f.</returns>
        public bool Update(int id, List<string> elementoftable)
        {
            Edzo regiEdzo = DbConnect.Peldany.GetOneEdzo(id);
            if (regiEdzo != null)
            {
                this.Update(id, this.OrvosUpdate(regiEdzo, elementoftable));
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Get versenyzo,first Non crud method.
        /// </summary>
        /// <param name="name">a.</param>
        /// <returns>Versenyzo.</returns>
        public IQueryable VersenyzoListazasaEdzoiNevekSzerint(string name)
        {
            VersenyzoRepository repoJUmp = new VersenyzoRepository();
            IQueryable<Edzo> edzok = this.GetAll();
            IQueryable<Versenyzo> versenyzoK = repoJUmp.GetAll();

            var q = edzok.Join(
                versenyzoK,
                edzo => edzo.Edzo_ID,
                versenyzo => versenyzo.Edzo_ID,
                (edzo, versenyzo) => new
                {
                    EdzoNev = edzo.Nev,
                    VersenyzoNeve = versenyzo.Nev,
                    versenyzo.Klub,
                    versenyzo.Suly,
                }).Where(x => x.EdzoNev == name).OrderBy(y => y.VersenyzoNeve).AsQueryable();

            foreach (var item in q)
            {
                if (item.EdzoNev != string.Empty)
                {
                    return q;
                }
                else
                {
                    throw new FormatException();
                }
            }

            return null; // nem jut el
        }
   }
}
