﻿// <copyright file="LogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SportEvent.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Moq;
    using NUnit.Framework;
    using SportEvent.Data;
    using SportEvent.Repository;

    /// <summary>
    /// Logic test class.
    /// </summary>
    [TestFixture]
    public class LogicTest
    {
        private Mock<IRepository<Versenyzo>> mockedVersenyzoRepo = new Mock<IRepository<Versenyzo>>();
        private Mock<IRepository<Edzo>> mockedEdzoRepo = new Mock<IRepository<Edzo>>();
        private Mock<IRepository<Vizsgalat>> mockedVizsgalatRepo = new Mock<IRepository<Vizsgalat>>();
        private Mock<IRepository<Orvos>> mockedOrvosRepo = new Mock<IRepository<Orvos>>();
        private Mock<ILogic> mockedLogic = new Mock<ILogic>();

        private List<Versenyzo> versenyzos;
        private List<Edzo> edzos;
        private List<Vizsgalat> vizsgalats;
        private List<Orvos> orvos;

        /// <summary>
        /// Gets or sets MockedVersenyzoRepo.
        /// </summary>
        public Mock<IRepository<Versenyzo>> MockedVersenyzoRepo { get => this.mockedVersenyzoRepo; set => this.mockedVersenyzoRepo = value; }

        /// <summary>
        /// Gets or sets MockedEdzoRepo.
        /// </summary>
        public Mock<IRepository<Edzo>> MockedEdzoRepo { get => this.mockedEdzoRepo; set => this.mockedEdzoRepo = value; }

        /// <summary>
        /// Gets or sets MockedVizsgalatRepo.
        /// </summary>
        public Mock<IRepository<Vizsgalat>> MockedVizsgalatRepo { get => this.mockedVizsgalatRepo; set => this.mockedVizsgalatRepo = value; }

        /// <summary>
        /// Gets or sets MockedOrvosRepo.
        /// </summary>
        public Mock<IRepository<Orvos>> MockedOrvosRepo { get => this.mockedOrvosRepo; set => this.mockedOrvosRepo = value; }

        /// <summary>
        /// Gets or sets MockedLogic.
        /// </summary>
        public Mock<ILogic> MockedLogic { get => this.mockedLogic; set => this.mockedLogic = value; }

        /// <summary>
        /// Setup the fake databases.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            this.versenyzos = new List<Versenyzo>
            {
                new Versenyzo() { V_ID = 1,  Nev = "Nagy Aladár", Nem = "férfi", Kor = 30 },
                new Versenyzo() { V_ID = 2, Nev = "Kis Gergő", Nem = "nő", },
                new Versenyzo() { V_ID = 3, Nev = "Horvát Balázs", Nem = "nő", },
            };

            this.orvos = new List<Orvos>
            {
                new Orvos() { Orvos_ID = 1, Nev = "DR Test", Szakirany = "Szakirany", Sportja = "sportja", VizsgaltEMar = "igen" },
                new Orvos() { Orvos_ID = 50, Nev = "DR", Szakirany = "Teszt",  Sportja = "balett", VizsgaltEMar = "nem" },
                new Orvos() { Orvos_ID = 655, Nev = " Test", Szakirany = "Teszt Sakirany",   VizsgaltEMar = "igen" },
            };

            this.edzos = new List<Edzo>
            {
                new Edzo() { Edzo_ID = 333, Nev = "asd", Kor = 45, Klub = "TSZK" },
                new Edzo() { Edzo_ID = 663, Nev = "Nagy Bela", Kor = 50, Klub = "FTC" },
                new Edzo() { Edzo_ID = 666, Nev = "Egresi Béla", Kor = 20, Klub = "asd" },
            };

            this.vizsgalats = new List<Vizsgalat>
            {
                new Vizsgalat() { Vizsg_ID = 2, V_ID = 18, Orvos_ID = 1, Eredmenye = "Megfelelt", Datuma = DateTime.Parse("2019.01.19") },
                new Vizsgalat() { Vizsg_ID = 222, V_ID = 2, Orvos_ID = 2, Eredmenye = "Megtagadva", Datuma = DateTime.Parse("2019.08.19") },
                new Vizsgalat() { Vizsg_ID = 25, V_ID = 5, Orvos_ID = 3, Eredmenye = "Eredmenye", Datuma = DateTime.Parse("2019.09.19") },
            };

            this.MockedVersenyzoRepo.Setup(m => m.GetAll()).Returns(this.versenyzos.AsQueryable());

            this.MockedEdzoRepo.Setup(m => m.GetAll()).Returns(this.edzos.AsQueryable());

            this.mockedOrvosRepo.Setup(m => m.GetAll()).Returns(this.orvos.AsQueryable());

            this.mockedVizsgalatRepo.Setup(m => m.GetAll()).Returns(this.vizsgalats.AsQueryable());
        }

        /// <summary>
        /// Test read method for versenyzo.
        /// </summary>
        [Test]
        public void ReadVersenyzo()
        {
            this.mockedVersenyzoRepo.Setup(v => v.GetAll()).Returns(this.versenyzos.AsQueryable());
            var result = this.mockedVersenyzoRepo.Object.GetAll();

            Assert.That(result.Count(), Is.EqualTo(3));
            Assert.That(result.Select(x => x.Nev), Does.Contain("Kis Gergő"));
            Assert.That(result.Select(x => x.Kor), Does.Contain(30));
            this.mockedVersenyzoRepo.Verify(m => m.GetAll(), Times.Once);
        }

        /// <summary>
        /// Test read method for orvos.
        /// </summary>
        [Test]
        public void ReadOrvos()
        {
            this.mockedOrvosRepo.Setup(o => o.GetAll()).Returns(this.orvos.AsQueryable());
            var result = this.mockedOrvosRepo.Object.GetAll();

            Assert.That(result.Count(), Is.EqualTo(3));
            Assert.That(result.Select(x => x.Nev), Does.Contain("DR"));
            Assert.That(result.Select(x => x.Szakirany), Does.Contain("Szakirany"));
            this.mockedOrvosRepo.Verify(m => m.GetAll(), Times.Once);
        }

        /// <summary>
        /// Test read method for edzo.
        /// </summary>
        [Test]
        public void ReadEdzo()
        {
            this.mockedEdzoRepo.Setup(v => v.GetAll()).Returns(this.edzos.AsQueryable());
            var result = this.mockedEdzoRepo.Object.GetAll();

            Assert.That(result.Count(), Is.EqualTo(3));
            Assert.That(result.Select(x => x.Klub), Does.Contain("FTC"));
            Assert.That(result.Select(x => x.Kor), Does.Contain(20));
            this.mockedEdzoRepo.Verify(m => m.GetAll(), Times.Once);
        }

        /// <summary>
        /// Test read method for vizsgalat.
        /// </summary>
        [Test]
        public void ReadVizsgalat()
        {
            this.mockedVizsgalatRepo.Setup(v => v.GetAll()).Returns(this.vizsgalats.AsQueryable());
            var result = this.mockedVizsgalatRepo.Object.GetAll();

            Assert.That(result.Count(), Is.EqualTo(3));
            Assert.That(result.Select(x => x.Vizsg_ID), Does.Contain(2));
            Assert.That(result.Select(x => x.Eredmenye), Does.Contain("Megfelelt"));
            this.mockedVizsgalatRepo.Verify(m => m.GetAll(), Times.Once);
        }

        ////--------------------------------------------------------

        /// <summary>
        /// Test for delet Versenyzo.
        /// </summary>
        [Test]
        public void DeletVersenyzo()
        {
            this.MockedVersenyzoRepo.Setup(m => m.Delete2(1)).Returns(true);
            this.MockedVersenyzoRepo.Setup(m => m.Delete2(1)).Returns(this.versenyzos.Remove(this.versenyzos[0]));
            var result = this.MockedVersenyzoRepo.Object.Delete2(1);

            Assert.That(result, Is.True);
            Assert.That(this.versenyzos.Count(), Is.EqualTo(2));
            this.MockedVersenyzoRepo.Verify(m => m.Delete2(1), Times.Once);
        }

        /// <summary>
        /// Test for delet Edzo.
        /// </summary>
        [Test]
        public void DeletEdzo()
        {
            this.mockedEdzoRepo.Setup(m => m.Delete2(1)).Returns(true);
            this.mockedEdzoRepo.Setup(m => m.Delete2(1)).Returns(this.edzos.Remove(this.edzos[0]));
            var result = this.mockedEdzoRepo.Object.Delete2(1);

            Assert.That(result, Is.True);
            Assert.That(this.edzos.Count(), Is.EqualTo(2));
            this.mockedEdzoRepo.Verify(m => m.Delete2(1), Times.Once);
        }

        /// <summary>
        /// Test for delet Orvos.
        /// </summary>
        [Test]
        public void DeletOrvos()
        {
            this.MockedOrvosRepo.Setup(m => m.Delete2(1)).Returns(true);
            this.MockedVersenyzoRepo.Setup(m => m.Delete2(1)).Returns(this.orvos.Remove(this.orvos[0]));
            var result = this.MockedOrvosRepo.Object.Delete2(1);

            Assert.That(result, Is.True);
            Assert.That(this.orvos.Count(), Is.EqualTo(2));
            this.MockedOrvosRepo.Verify(m => m.Delete2(1), Times.Once);
        }

        /// <summary>
        /// Test for delet vizsgalat.
        /// </summary>
        [Test]
        public void DeletVizsgalat()
        {
            this.mockedVizsgalatRepo.Setup(m => m.Delete2(1)).Returns(true);
            this.mockedVizsgalatRepo.Setup(m => m.Delete2(1)).Returns(this.vizsgalats.Remove(this.vizsgalats[0]));
            var result = this.mockedVizsgalatRepo.Object.Delete2(1);

            Assert.That(result, Is.True);
            Assert.That(this.vizsgalats.Count(), Is.EqualTo(2));
            this.mockedVizsgalatRepo.Verify(m => m.Delete2(1), Times.Once);
        }

        /// <summary>
        /// Test for insert Versenyzo.
        /// </summary>
        [Test]
        public void InsertVersenyzo()
        {
            List<string> element = new List<string>() { "5",  "nev", "nem" };
            this.MockedVersenyzoRepo.Setup(m => m.Insert(element)).Verifiable();
            this.MockedVersenyzoRepo.Object.Insert(element);
            Assert.That(this.versenyzos.Count(), Is.EqualTo(3));
            this.MockedVersenyzoRepo.Verify(m => m.Insert(element), Times.Once);
        }

        /// <summary>
        /// Test for insert Orvos.
        /// </summary>
        [Test]
        public void InsertOrvos()
        {
            List<string> element = new List<string>() { "5", "nev", "szakirany" };
            this.MockedOrvosRepo.Setup(m => m.Insert(element)).Verifiable();
            this.MockedOrvosRepo.Object.Insert(element);
            Assert.That(this.versenyzos.Count(), Is.EqualTo(3));
            this.MockedOrvosRepo.Verify(m => m.Insert(element), Times.Once);
        }

        /// <summary>
        /// Test for insert Edzo.
        /// </summary>
        [Test]
        public void InsertEdzo()
        {
            List<string> element = new List<string>() { "5", "nev", "klub" };
            this.mockedEdzoRepo.Setup(m => m.Insert(element)).Verifiable();
            this.mockedEdzoRepo.Object.Insert(element);
            Assert.That(this.versenyzos.Count(), Is.EqualTo(3));
            this.mockedEdzoRepo.Verify(m => m.Insert(element), Times.Once);
        }

        /// <summary>
        /// Test for insert Vizsgalat.
        /// </summary>
        [Test]
        public void InsertVizsgalat()
        {
            List<string> element = new List<string>() { "5", "33", "4" };
            this.MockedVizsgalatRepo.Setup(m => m.Insert(element)).Verifiable();
            this.MockedVizsgalatRepo.Object.Insert(element);
            Assert.That(this.versenyzos.Count(), Is.EqualTo(3));
            this.MockedVizsgalatRepo.Verify(m => m.Insert(element), Times.Once);
        }

        /// <summary>
        /// Test for update Versenyzo.
        /// </summary>
        [Test]
        public void UpdateVersenyzo()
        {
            List<string> canUpdate = new List<string> { "1", "tezst", "5" };
            List<string> cantUpdate = new List<string> { "20", "teszt", "5" };

            this.MockedVersenyzoRepo.Setup(m => m.Update(1, canUpdate)).Returns(true);
            this.MockedVersenyzoRepo.Setup(m => m.Update(20, cantUpdate)).Returns(false);

            var resultIsTrue = this.MockedVersenyzoRepo.Object.Update(1, canUpdate);
            var resultIsFalse = this.MockedVersenyzoRepo.Object.Update(20, cantUpdate);

            Assert.That(resultIsTrue, Is.True);
            Assert.That(resultIsFalse, Is.False);
        }

        /// <summary>
        /// Test for update Orvos.
        /// </summary>
        [Test]
        public void UpdateOrvos()
        {
            List<string> canUpdate = new List<string> { "1", "Beluga", "5" };
            List<string> cantUpdate = new List<string> { "20", "Beluga", "5" };

            this.MockedOrvosRepo.Setup(m => m.Update(1, canUpdate)).Returns(true);
            this.MockedVizsgalatRepo.Setup(m => m.Update(20, cantUpdate)).Returns(false);

            var resultIsTrue = this.MockedVizsgalatRepo.Object.Update(1, canUpdate);
            var resultIsFalse = this.MockedVizsgalatRepo.Object.Update(20, cantUpdate);

            Assert.That(resultIsTrue, Is.True);
            Assert.That(resultIsFalse, Is.False);
        }

        /// <summary>
        /// Test for update Vizsgalat.
        /// </summary>
        [Test]
        public void UpdateEdzo()
        {
            List<string> canUpdate = new List<string> { "1", "Beluga", "5" };
            List<string> cantUpdate = new List<string> { "20", "Beluga", "5" };

            this.MockedVizsgalatRepo.Setup(m => m.Update(1, canUpdate)).Returns(true);
            this.MockedVizsgalatRepo.Setup(m => m.Update(20, cantUpdate)).Returns(false);

            var resultIsTrue = this.MockedVizsgalatRepo.Object.Update(1, canUpdate);
            var resultIsFalse = this.MockedVizsgalatRepo.Object.Update(20, cantUpdate);

            Assert.That(resultIsTrue, Is.True);
            Assert.That(resultIsFalse, Is.False);
        }

        /// <summary>
        /// Test for update Vizsgalat.
        /// </summary>
        [Test]
        public void UpdateVizsgalat()
        {
            List<string> canUpdate = new List<string> { "5", "33", "4" };
            List<string> cantUpdate = new List<string> { "5", "33", "4" };

            this.MockedVizsgalatRepo.Setup(m => m.Update(1, canUpdate)).Returns(true);
            this.MockedVizsgalatRepo.Setup(m => m.Update(20, cantUpdate)).Returns(false);

            var resultIsTrue = this.MockedVizsgalatRepo.Object.Update(1, canUpdate);
            var resultIsFalse = this.MockedVizsgalatRepo.Object.Update(20, cantUpdate);

            Assert.That(resultIsTrue, Is.True);
            Assert.That(resultIsFalse, Is.False);
        }

        /// <summary>
        /// Test for the first NON CRUD method.
        /// </summary>
        [Test]
        public void IndulhatVersenyzoAVersenyenTest()
        {
            IQueryable<Versenyzo> versenyzok = this.mockedVersenyzoRepo.Object.GetAll();
            IQueryable<Vizsgalat> vizsgalatok = this.MockedVizsgalatRepo.Object.GetAll();

            var q = versenyzok.Join(
                 vizsgalatok,
                 versenyzo => versenyzo.V_ID,
                 vizsgalat => vizsgalat.V_ID,
                 (versenyzo, vizsgalat) => new
                 {
                     VersenyzoNeve = versenyzo.Nev,
                     versenyzo.Klub,
                     versenyzo.Küzdelem,
                     versenyzo.Formagy,
                     vizsgalat.Eredmenye,
                 }).Where(x => x.Eredmenye != "Megtagadva").OrderBy(y => y.VersenyzoNeve).AsQueryable();

            this.MockedLogic.Setup(m => m.VersenyzoEdzoje()).Returns(q);
            var result = this.MockedLogic.Object.VersenyzoEdzoje().AsQueryable();

            Assert.That(result, Is.EqualTo(q));
        }

        /// <summary>
        /// Test for NON CRUD method,which give us orvosok.
        /// </summary>
        [Test]
        public void OrvosListazasaMegtagadottVizsgalatiEredmenyAlapjan()
        {
            IQueryable<Orvos> orvosok = this.MockedOrvosRepo.Object.GetAll();
            IQueryable<Vizsgalat> vizsgalatok = this.MockedVizsgalatRepo.Object.GetAll();

            var q = orvosok.Join(
               vizsgalatok,
               orvos => orvos.Orvos_ID,
               vizsgalat => vizsgalat.Orvos_ID,
               (orvos, vizsgalat) => new
               {
                   OrvoNev = orvos.Nev,
                   vizsgalat = vizsgalat.Eredmenye,
                   vizsgalat.Datuma,
               }).Where(x => x.vizsgalat == "Megtagadva").OrderBy(y => y.OrvoNev).AsQueryable();

            this.MockedLogic.Setup(m => m.VersenyzoListOrvosListazasaMegtagadottVizsgalatiEredmenyAlapjanzasaEdzoiNevekSzerint()).Returns(q);
            var result = this.MockedLogic.Object.VersenyzoListOrvosListazasaMegtagadottVizsgalatiEredmenyAlapjanzasaEdzoiNevekSzerint().AsQueryable();

            Assert.That(result, Is.EqualTo(q));
        }

        /// <summary>
        /// Test for NON CRUD method,which give us versenyzok.
        /// </summary>
        /// <param name="name">String edzo name.</param>
        [TestCase("Barát Réka")]
        public void VersenyzoListazasaEdzoiNevekSzerintTest(string name)
        {
            IQueryable<Edzo> edzok = this.mockedEdzoRepo.Object.GetAll();
            IQueryable<Versenyzo> versenyzoK = this.mockedVersenyzoRepo.Object.GetAll();

            var q = edzok.Join(
               versenyzoK,
               edzo => edzo.Edzo_ID,
               versenyzo => versenyzo.Edzo_ID,
               (edzo, versenyzo) => new
               {
                   EdzoNev = edzo.Nev,
                   VersenyzoNeve = versenyzo.Nev,
                   versenyzo.Klub,
                   versenyzo.Suly,
               }).Where(x => x.EdzoNev == name).OrderBy(y => y.VersenyzoNeve).AsQueryable();

            this.MockedLogic.Setup(m => m.VersenyzoListazasaEdzoiNevekSzerint(name)).Returns(q);
            var result = this.MockedLogic.Object.VersenyzoListazasaEdzoiNevekSzerint(name).AsQueryable();

            Assert.That(result, Is.EqualTo(q));
        }
    }
}
