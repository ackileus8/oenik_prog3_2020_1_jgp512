var dir_691569a03afcabfd94a9a52492c3ac2a =
[
    [ "SportEvent", "dir_1ba3c087747892382360648e47587d82.html", "dir_1ba3c087747892382360648e47587d82" ],
    [ "SportEvent.Data", "dir_2d23de6b0f3195498f8ca6db3d4583b5.html", "dir_2d23de6b0f3195498f8ca6db3d4583b5" ],
    [ "SportEvent.Logic", "dir_3a81a3e9c6abf9984dc0a62c1bf1947b.html", "dir_3a81a3e9c6abf9984dc0a62c1bf1947b" ],
    [ "SportEvent.Logic.Tests", "dir_32aee9945e4a64c14cf175672ecfad5d.html", "dir_32aee9945e4a64c14cf175672ecfad5d" ],
    [ "SportEvent.Repository", "dir_4400fa50c12450c6c68a76672caa504e.html", "dir_4400fa50c12450c6c68a76672caa504e" ]
];