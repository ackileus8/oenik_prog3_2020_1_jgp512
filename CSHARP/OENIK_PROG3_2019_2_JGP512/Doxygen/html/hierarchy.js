var hierarchy =
[
    [ "SportEvent.Data.DataConnection", "class_sport_event_1_1_data_1_1_data_connection.html", null ],
    [ "DbContext", null, [
      [ "SportEvent.Data::DatabaseEntities", "class_sport_event_1_1_data_1_1_database_entities.html", null ]
    ] ],
    [ "SportEvent.Data.Edzo", "class_sport_event_1_1_data_1_1_edzo.html", null ],
    [ "SportEvent.Logic.ILogic", "interface_sport_event_1_1_logic_1_1_i_logic.html", [
      [ "SportEvent.Logic.Logic", "class_sport_event_1_1_logic_1_1_logic.html", null ]
    ] ],
    [ "SportEvent.Repository.IRepository< T >", "interface_sport_event_1_1_repository_1_1_i_repository.html", null ],
    [ "SportEvent.Repository.IRepository< Edzo >", "interface_sport_event_1_1_repository_1_1_i_repository.html", [
      [ "SportEvent.Repository.EdzoRepository", "class_sport_event_1_1_repository_1_1_edzo_repository.html", null ]
    ] ],
    [ "SportEvent.Repository.IRepository< Orvos >", "interface_sport_event_1_1_repository_1_1_i_repository.html", [
      [ "SportEvent.Repository.OrvosRepository", "class_sport_event_1_1_repository_1_1_orvos_repository.html", null ]
    ] ],
    [ "SportEvent.Repository.IRepository< Versenyzo >", "interface_sport_event_1_1_repository_1_1_i_repository.html", [
      [ "SportEvent.Repository.VersenyzoRepository", "class_sport_event_1_1_repository_1_1_versenyzo_repository.html", null ]
    ] ],
    [ "SportEvent.Repository.IRepository< Vizsgalat >", "interface_sport_event_1_1_repository_1_1_i_repository.html", [
      [ "SportEvent.Repository.VizsgalatRepository", "class_sport_event_1_1_repository_1_1_vizsgalat_repository.html", null ]
    ] ],
    [ "SportEvent.Logic.Tests.LogicTest", "class_sport_event_1_1_logic_1_1_tests_1_1_logic_test.html", null ],
    [ "SportEvent.Data.Orvos", "class_sport_event_1_1_data_1_1_orvos.html", null ],
    [ "SportEvent.Data.Versenyzo", "class_sport_event_1_1_data_1_1_versenyzo.html", null ],
    [ "SportEvent.Data.Vizsgalat", "class_sport_event_1_1_data_1_1_vizsgalat.html", null ]
];