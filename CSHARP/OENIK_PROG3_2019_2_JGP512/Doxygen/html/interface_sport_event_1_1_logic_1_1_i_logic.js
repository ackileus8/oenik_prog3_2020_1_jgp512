var interface_sport_event_1_1_logic_1_1_i_logic =
[
    [ "DeletEdzo2", "interface_sport_event_1_1_logic_1_1_i_logic.html#a3cd688c160b01a3110a8d3c8c9a98108", null ],
    [ "DeletOrvos2", "interface_sport_event_1_1_logic_1_1_i_logic.html#acb4b362b2d274c8e125b7808e1a4e76e", null ],
    [ "DeletVersenyzo2", "interface_sport_event_1_1_logic_1_1_i_logic.html#a96bb2b1375aa25816d558226e2819511", null ],
    [ "DeletVizsgalat2", "interface_sport_event_1_1_logic_1_1_i_logic.html#a4c643a17e405132ed53912d8a6f95e0d", null ],
    [ "GetStringBuilderEdzo", "interface_sport_event_1_1_logic_1_1_i_logic.html#a86cee581f081f620f502c0707c19a4e2", null ],
    [ "GetStringBuilderVizsgalat", "interface_sport_event_1_1_logic_1_1_i_logic.html#a9f40321665bda9c58060c4c08fa22b59", null ],
    [ "GetTableContentsEdzo", "interface_sport_event_1_1_logic_1_1_i_logic.html#a47ad2b89e915e96c45a43aebe33f9d4c", null ],
    [ "GetTableContentsOrvos", "interface_sport_event_1_1_logic_1_1_i_logic.html#a18acd9cd44dcff365493c91bbdb9e28f", null ],
    [ "InsertEdzo", "interface_sport_event_1_1_logic_1_1_i_logic.html#a7d9a507ea211f7e57c91220e8c77660e", null ],
    [ "InsertOrvos", "interface_sport_event_1_1_logic_1_1_i_logic.html#a9886703e7dc94a54ff5194c315de4540", null ],
    [ "InsertVersenyzo", "interface_sport_event_1_1_logic_1_1_i_logic.html#a29e8a0d48a2688ae4ebe8b80627841fe", null ],
    [ "InsertVizsgalat", "interface_sport_event_1_1_logic_1_1_i_logic.html#ae003fe5d7dce91b2ad33cb87c4206dbf", null ],
    [ "UpdateEdzo", "interface_sport_event_1_1_logic_1_1_i_logic.html#a83b59323e9761fef4eecd2a5a290b048", null ],
    [ "UpdateOrvos", "interface_sport_event_1_1_logic_1_1_i_logic.html#a89719525b9292c5290a499e532be01b4", null ],
    [ "UpdateVersenyzo", "interface_sport_event_1_1_logic_1_1_i_logic.html#afccbd66ca1fa40de3deccdfcb6152966", null ],
    [ "UpdateVizsgalat", "interface_sport_event_1_1_logic_1_1_i_logic.html#ae35a7ab504f22b406995935ad158d393", null ]
];