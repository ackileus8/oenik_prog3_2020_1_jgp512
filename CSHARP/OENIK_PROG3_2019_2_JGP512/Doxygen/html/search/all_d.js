var searchData=
[
  ['verseny_66',['Verseny',['../class_sport_event_1_1_logic_1_1_logic.html#aedadfdc9710ce9df43ce3bcb8a2e636d',1,'SportEvent::Logic::Logic']]],
  ['versenyzo_67',['Versenyzo',['../class_sport_event_1_1_data_1_1_versenyzo.html',1,'SportEvent::Data']]],
  ['versenyzoedzoje_68',['VersenyzoEdzoje',['../class_sport_event_1_1_logic_1_1_logic.html#ab436de0f629def5aa4b5fae586d2bdad',1,'SportEvent::Logic::Logic']]],
  ['versenyzolistazasaedzoinevekszerint_69',['VersenyzoListazasaEdzoiNevekSzerint',['../class_sport_event_1_1_logic_1_1_logic.html#a98d989f356d61d0116c6d9d552b58ad5',1,'SportEvent.Logic.Logic.VersenyzoListazasaEdzoiNevekSzerint()'],['../class_sport_event_1_1_repository_1_1_edzo_repository.html#aaec4963e463867da37a4f9ad6ba5ca19',1,'SportEvent.Repository.EdzoRepository.VersenyzoListazasaEdzoiNevekSzerint()']]],
  ['versenyzolistorvoslistazasamegtagadottvizsgalatieredmenyalapjanzasaedzoinevekszerint_70',['VersenyzoListOrvosListazasaMegtagadottVizsgalatiEredmenyAlapjanzasaEdzoiNevekSzerint',['../class_sport_event_1_1_logic_1_1_logic.html#a977ae5869218362e8d9f92d1959e0496',1,'SportEvent::Logic::Logic']]],
  ['versenyzorepository_71',['VersenyzoRepository',['../class_sport_event_1_1_repository_1_1_versenyzo_repository.html',1,'SportEvent::Repository']]],
  ['versenyzoupdate_72',['VersenyzoUpdate',['../class_sport_event_1_1_repository_1_1_versenyzo_repository.html#ab27c46f74421175aef9a0eb4faa27d31',1,'SportEvent::Repository::VersenyzoRepository']]],
  ['vizsgalat_73',['Vizsgalat',['../class_sport_event_1_1_data_1_1_vizsgalat.html',1,'SportEvent::Data']]],
  ['vizsgalatrepository_74',['VizsgalatRepository',['../class_sport_event_1_1_repository_1_1_vizsgalat_repository.html',1,'SportEvent::Repository']]],
  ['vizsgalatupdate_75',['VizsgalatUpdate',['../class_sport_event_1_1_repository_1_1_vizsgalat_repository.html#a90950b67475d6cbc7f03d866798609e5',1,'SportEvent::Repository::VizsgalatRepository']]],
  ['vizsghelper_76',['VizsgHelper',['../class_sport_event_1_1_logic_1_1_logic.html#ab273700c6d17f54c1b2f49ba490c40d1',1,'SportEvent::Logic::Logic']]]
];
