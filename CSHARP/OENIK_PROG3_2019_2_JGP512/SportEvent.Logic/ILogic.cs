﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SportEvent.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// ILogic interface base methods.
    /// </summary>
    public interface ILogic
    {
        //-------------------------------------------------------------Insert kezdete--------------------------------------------------------------

       /// <summary>
       /// Insert versenyzo.
       /// </summary>
       /// <param name="nev">nev.</param>
        void InsertVersenyzo(List<string> nev);

       /// <summary>
       /// Insert orvos.
       /// </summary>
       /// <param name="nev">nev.</param>
        void InsertOrvos(List<string> nev);

      /// <summary>
      /// insert edzo.
      /// </summary>
      /// <param name="nev">nev.</param>
        void InsertEdzo(List<string> nev);

        /// <summary>
        /// Insert vizsgalat.
        /// </summary>
        /// <param name="nev">a.</param>
        void InsertVizsgalat(List<string> nev);

        // --------------------------------------------------------Insert vege-------------------------------------------------------------------------

            /// <summary>
            /// Delet method for versenyzo.
            /// </summary>
            /// <param name="id">i.</param>
            /// <returns>t/f.</returns>
        bool DeletVersenyzo2(int id);

        /// <summary>
        /// Delet method for Vizsgalat.
        /// </summary>
        /// <param name="id">n.</param>
        /// <returns>T/F.</returns>
        bool DeletVizsgalat2(int id);

        /// <summary>
        /// Delet method for Edzo.
        /// </summary>
        /// <param name="id">n.</param>
        /// <returns>T/F.</returns>
        bool DeletEdzo2(int id);

        /// <summary>
        /// Delet method for ORvos.
        /// </summary>
        /// <param name="id">n.</param>
        /// <returns>t/f.</returns>
        bool DeletOrvos2(int id);

        // --------------------------------------------------------Delet vege-----------------------------------------------------

        /// <summary>
        /// Make string from vizsgalat.
        /// </summary>
        /// <returns>StringBuilder.</returns>
        StringBuilder GetStringBuilderVizsgalat();

        /// <summary>
        /// Make string from edzo.
        /// </summary>
        /// <returns>StringBuilder.</returns>
        StringBuilder GetStringBuilderEdzo();

        /// <summary>
        /// Make string from orvos.
        /// </summary>
        /// <returns>StringBuilder.</returns>
        StringBuilder GetTableContentsOrvos();

        /// <summary>
        /// Make string from vizsgalat.
        /// </summary>
        /// <returns>StringBuilder.</returns>
        StringBuilder GetTableContentsEdzo();

        // --------------------------------------------------- -----------------------------------------------------------------------

            /// <summary>
            /// Update method for versenyzo.
            /// </summary>
            /// <param name="id">b.</param>
            /// <param name="updateVersenyzo">n.</param>
            /// <returns>T/F.</returns>
        bool UpdateVersenyzo(int id, List<string> updateVersenyzo);

      /// <summary>
      /// Update for orvos.
      /// </summary>
      /// <param name="id">a.</param>
      /// <param name="updateOrvos">aa.</param>
      /// <returns>t/f.</returns>
        bool UpdateOrvos(int id, List<string> updateOrvos);

        /// <summary>
        /// Update method for edzo.
        /// </summary>
        /// <param name="id">a.</param>
        /// <param name="updateEdzo">aa.</param>
        /// <returns>T/f.</returns>
        bool UpdateEdzo(int id, List<string> updateEdzo);

        /// <summary>
        /// update method for Viszgalat.
        /// </summary>
        /// <param name="id">.</param>
        /// <param name="updateVizsgalat">f.</param>
        /// <returns>t/f.</returns>
        bool UpdateVizsgalat(int id, List<string> updateVizsgalat);

        // -------------------------------------------------------------Non CRUD-------------------------------------------------------------

        /// <summary>
        /// Provides the first non CRUD method for Logic.
        /// </summary>
        /// <param name="nev">Versenyzo name.</param>
        /// <returns>Versenyzok in IQueryable form.</returns>
        IQueryable VersenyzoListazasaEdzoiNevekSzerint(string nev);

        /// <summary>
        /// Provides the second non CRUD method for Logic.
        /// </summary>
        /// <returns>Versenyzok in IQueryable form.</returns>
        IQueryable VersenyzoListOrvosListazasaMegtagadottVizsgalatiEredmenyAlapjanzasaEdzoiNevekSzerint();

        /// <summary>
        /// Provides the third non CRUD method for Logic.
        /// </summary>
        /// <returns>Edzok in IQueryable form.</returns>
        IQueryable VersenyzoEdzoje();
    }
}
