﻿// <copyright file="Logic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SportEvent.Logic
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Text;
    using System.Xml.Linq;
    using SportEvent.Repository;

    /// <summary>
    /// Contains logic database methods.
    /// </summary>
    public class Logic : ILogic
    {
        private readonly Repository.VersenyzoRepository verseny = new Repository.VersenyzoRepository();
        private readonly SportEvent.Repository.EdzoRepository edzo = new Repository.EdzoRepository();
        private readonly SportEvent.Repository.VizsgalatRepository vizs = new Repository.VizsgalatRepository();
        private readonly SportEvent.Repository.OrvosRepository orvos = new Repository.OrvosRepository();

        /// <summary>
        /// gets.
        /// </summary>
        public virtual SportEvent.Repository.EdzoRepository EdzoHelper
        {
            get { return this.edzo; }
        }

        /// <summary>
        /// gets.
        /// </summary>
        public virtual SportEvent.Repository.VersenyzoRepository Helper
        {
            get { return this.Verseny; }
        }

        /// <summary>
        /// gets.
        /// </summary>
        public virtual SportEvent.Repository.VizsgalatRepository VizsgHelper
        {
            get { return this.vizs; }
        }

        /// <summary>
        /// gets.
        /// </summary>
        public virtual SportEvent.Repository.OrvosRepository OrvosHelper
        {
            get { return this.orvos; }
        }

        /// <summary>
        /// Gets.
        /// </summary>
        public VersenyzoRepository Verseny => this.verseny;

        /// <summary>
        /// dasdasd.
        /// </summary>
        /// <returns>Ture or False.</returns>
        public List<string> GetAllTableNames()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Insert versenyzo method.
        /// </summary>
        /// <param name="nev">a.</param>
        public void InsertVersenyzo(List<string> nev)
        {
          this.Helper.Insert(nev);
        }

        /// <summary>
        /// Insert edzo method.
        /// </summary>
        /// <param name="nev">a.</param>
        public void InsertEdzo(List<string> nev)
        {
            this.EdzoHelper.Insert(nev);
        }

        /// <summary>
        /// Isnert orvos method.
        /// </summary>
        /// <param name="nev">as.</param>
        public void InsertOrvos(List<string> nev)
        {
            this.OrvosHelper.Insert(nev);
        }

        /// <summary>
        /// Insert Vizsgalat method.
        /// </summary>
        /// <param name="nev">a.</param>
        public void InsertVizsgalat(List<string> nev)
        {
            this.VizsgHelper.Insert(nev);
        }

        // --------------------------------------------------------------------------DELETE--------------------------------------------------------

        /// <summary>
        /// Delete versenyzo method.
        /// </summary>
        /// <param name="id">a.</param>
        /// <returns>t/f.</returns>
        public bool DeletVersenyzo2(int id)
        {
           return this.Helper.Delete2(id);
        }

        /// <summary>
        /// Delete edzo method.
        /// </summary>
        /// <param name="id">a.</param>
        /// <returns>t/f.</returns>
        public bool DeletEdzo2(int id)
        {
            return this.EdzoHelper.Delete2(id);
        }

        /// <summary>
        /// Delete orvos method.
        /// </summary>
        /// <param name="id">a.</param>
        /// <returns>t/f.</returns>
        public bool DeletOrvos2(int id)
        {
            return this.OrvosHelper.Delete2(id);
        }

        /// <summary>
        /// Delete vizsgalat method.
        /// </summary>
        /// <param name="id">a.</param>
        /// <returns>t/f.</returns>
        public bool DeletVizsgalat2(int id)
        {
            return this.VizsgHelper.Delete2(id);
        }

        //--------------------------------------------------------------------------StringBuilder--------------------------------------------------------

        /// <summary>
        /// Get table from db.
        /// </summary>
        /// <returns>StringBuilder.</returns>
        public StringBuilder GetStringBuilder()
        {
            return this.Helper.GetTableContents();
        }

        /// <summary>
        /// Get table from db.
        /// </summary>
        /// <returns>StringBuilder.</returns>
        public StringBuilder GetStringBuilderVizsgalat()
        {
            return this.VizsgHelper.GetTableContents();
        }

        /// <summary>
        /// Get table from db.
        /// </summary>
        /// <returns>StringBuilder.</returns>
        public StringBuilder GetStringBuilderEdzo()
        {
            return this.EdzoHelper.GetTableContents();
        }

        /// <summary>
        /// Get table from db.
        /// </summary>
        /// <returns>StringBuilder.</returns>
        public StringBuilder GetTableContentsOrvos()
        {
            return this.OrvosHelper.GetTableContents();
        }

        /// <summary>
        /// Get table from db.
        /// </summary>
        /// <returns>StringBuilder.</returns>
        public StringBuilder GetTableContentsEdzo()
        {
            return this.EdzoHelper.GetTableContents();
        }

        //---------------------------------------------------------update-------------------------------------------

        /// <summary>
        /// Update method for versenyzo.
        /// </summary>
        /// <param name="id">a.</param>
        /// <param name="updateVersenyzo">f.</param>
        /// <returns>t/f.</returns>
        public bool UpdateVersenyzo(int id, List<string> updateVersenyzo)
        {
            return this.Verseny.Update(id, updateVersenyzo);
        }

        /// <summary>
        /// Update orvos method.
        /// </summary>
        /// <param name="id">g.</param>
        /// <param name="updateOrvos">r.</param>
        /// <returns>t/f.</returns>
        public bool UpdateOrvos(int id, List<string> updateOrvos)
        {
            return this.orvos.Update(id, updateOrvos);
        }

        /// <summary>
        /// update edzo method.
        /// </summary>
        /// <param name="id">a.</param>
        /// <param name="updateEdzo">r.</param>
        /// <returns>t/f.</returns>
        public bool UpdateEdzo(int id, List<string> updateEdzo)
        {
            return this.edzo.Update(id, updateEdzo);
        }

        /// <summary>
        /// Update method for Vizsgalat.
        /// </summary>
        /// <param name="id">a.</param>
        /// <param name="updateVizsgalat">u.</param>
        /// <returns>t/f.</returns>
        public bool UpdateVizsgalat(int id, List<string> updateVizsgalat)
        {
            return this.vizs.Update(id, updateVizsgalat);
        }

        //-----------------------------------------------Nem CRUD -------------------------------------

        /// <summary>
        /// Return first non crud method.
        /// </summary>
        /// <param name="nev">a.</param>
        /// <returns>some versenyzo.</returns>
        public IQueryable VersenyzoListazasaEdzoiNevekSzerint(string nev)
        {
            return this.edzo.VersenyzoListazasaEdzoiNevekSzerint(nev);
        }

        /// <summary>
        /// Second non crud method.
        /// </summary>
        /// <returns>Versenyzo who passed the vizsgalat.</returns>
        public IQueryable VersenyzoListOrvosListazasaMegtagadottVizsgalatiEredmenyAlapjanzasaEdzoiNevekSzerint()
        {
            return this.orvos.OrvosListazasaMegtagadottVizsgalatiEredmenyAlapjan();
        }

        /// <summary>
        /// Who is the coach of the competitor.
        /// </summary>
        /// <returns>Edzo.</returns>
        public IQueryable VersenyzoEdzoje()
        {
            return this.Helper.IndulhatVersenyzoAVersenyen();
        }

        // -------------------------------------------------JAVA VEGPONT----------------------------------------------------------------

            /// <summary>
            /// Downloand xml from java.
            /// </summary>
            /// <returns>string.</returns>
        public string XmlFromURL()
        {
            string remoteUri = "http://localhost:8080/OENIK_PROG3_2019_2_JGP512/SportEvent?eredmeny=asd";
            WebClient myWebClient = new WebClient();
            byte[] myDataBuffer = myWebClient.DownloadData(remoteUri);
            string download = Encoding.ASCII.GetString(myDataBuffer);
            return download;
        }

        /// <summary>
        /// Split xml to cute form.
        /// </summary>
        /// <param name="tombSplit">a.</param>
        /// <returns>string block.</returns>
        public string[] SplitXml(string tombSplit)
        {
            string[] asd = new string[5];
            asd[0] = tombSplit.Split('\n')[2].Split('<')[1].Split('>')[0] + ':' + ' ' + tombSplit.Split('\n')[2].Split('>')[1].Split('<')[0];
            asd[1] = tombSplit.Split('\n')[3].Split('<')[1].Split('>')[0] + ':' + ' ' + tombSplit.Split('\n')[3].Split('>')[1].Split('<')[0];
            asd[2] = tombSplit.Split('\n')[4].Split('<')[1].Split('>')[0] + ':' + ' ' + tombSplit.Split('\n')[4].Split('>')[1].Split('<')[0];
            asd[3] = tombSplit.Split('\n')[5].Split('<')[1].Split('>')[0] + ':' + ' ' + tombSplit.Split('\n')[5].Split('>')[1].Split('<')[0];
            asd[4] = tombSplit.Split('\n')[6].Split('<')[1].Split('>')[0] + ':' + ' ' + tombSplit.Split('\n')[6].Split('>')[1].Split('<')[0].Split('T')[0];

            for (int i = 0; i < asd.Length; i++)
            {
                Console.WriteLine(asd[i]);
            }

            return asd;
        }
    }
}
