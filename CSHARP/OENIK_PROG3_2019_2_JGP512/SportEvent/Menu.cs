﻿// <copyright file="Menu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace SportEvent
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using SportEvent.Logic;

    /// <summary>
    /// Display the menu.
    /// </summary>
    internal class Menu
    {
        private static int index = 0;
        private readonly Logic.Logic logic = new Logic.Logic();

        /// <summary>
        /// Handel menu.
        /// </summary>
        public void Valami()
        {
            Console.Title = "SportEvent";
            List<string> menuItems = new List<string>()
            {
                "Listazas",
                "Hozzáadás",
                "Törles",
                "Beillesztés",
                "5",
                "6",
                "7",
                "Java Végpont",
                "Kilépés",
            };

            while (true)
            {
                string selectedMenuItem = DrawMenu(menuItems);

                if (selectedMenuItem == "Listazas")
                {
                    Console.Clear();
                    Console.WriteLine("Listázáshoz írjon be egy tábla nevet a következők közül:" + " (Versenyzo,Edzo,Orvos,Vizsgalat)");
                    string tablaNev = Console.ReadLine();
                    if (tablaNev == "Versenyzo")
                    {
                        Console.WriteLine(this.logic.GetStringBuilder());
                    }
                    else if (tablaNev == "Orvos")
                    {
                        Console.WriteLine(this.logic.GetTableContentsOrvos());
                    }
                    else if (tablaNev == "Vizsgalat")
                    {
                        Console.WriteLine(this.logic.GetStringBuilderVizsgalat());
                    }
                    else if (tablaNev == "Edzo")
                    {
                        Console.WriteLine(this.logic.GetStringBuilderEdzo());
                    }
                    else
                    {
                        Console.WriteLine("Rossz táblanév");
                    }

                    Console.ReadKey();
                    Console.Clear();
                }
                else if (selectedMenuItem == "Hozzáadás")
                {
                    Console.Clear();

                    Console.WriteLine("Írjon be egy tábla nevet,amihez hozzá szeretne adni adatot:" + " (Versenyzo,Edzo,Orvos,Vizsgalat)");
                    string category = Console.ReadLine();
                    if (category == "Versenyzo")
                    {
                        Console.WriteLine(this.logic.GetStringBuilder());
                        List<string> versenyzo = new List<string>();
                        Console.WriteLine("Írj be egy ID" + "(Kötelezően számot)");
                        versenyzo.Add(Console.ReadLine());
                        Console.WriteLine("Írj be egy nevet.");
                        versenyzo.Add(Console.ReadLine());
                        Console.WriteLine("Írj be egy Nemet.");
                        versenyzo.Add(Console.ReadLine());
                        Console.WriteLine("Írj be egy Súly." + "(Kötelezően számot)");
                        versenyzo.Add(Console.ReadLine());
                        Console.WriteLine("Írj be egy Kort." + "(Kötelezően számot)");
                        versenyzo.Add(Console.ReadLine());
                        Console.WriteLine("Írj be egy Klub.");
                        versenyzo.Add(Console.ReadLine());
                        Console.WriteLine("Írj be egy Ovfokozat.");
                        versenyzo.Add(Console.ReadLine());
                        Console.WriteLine("Írja be hogy indul-e küzdelembe.");
                        versenyzo.Add(Console.ReadLine());
                        Console.WriteLine("Írja be hogy indul-e formagyakorlatba.");
                        versenyzo.Add(Console.ReadLine());
                        Console.WriteLine("Írj be egy EdzoID-t." + "(Kötelezően meglévő EdzőID-t)");
                        versenyzo.Add(Console.ReadLine());
                        this.logic.InsertVersenyzo(versenyzo);
                        Console.WriteLine(this.logic.GetStringBuilder());
                    }
                    else if (category == "Vizsgalat")
                    {
                        Console.WriteLine(this.logic.GetStringBuilderVizsgalat());
                        List<string> vizsgalat = new List<string>();
                        Console.WriteLine("Írj be egy ID." + "(Számot)");
                        vizsgalat.Add(Console.ReadLine());
                        Console.WriteLine("Írj be egy  Versenyzo_ID-t." + "(Kötelezően meglévő VersenyzőID-t)");
                        vizsgalat.Add(Console.ReadLine());
                        Console.WriteLine("Írj be egy orvos_ID-t." + "(Kötelezően meglévő OrvosID-t)");
                        vizsgalat.Add(Console.ReadLine());
                        Console.WriteLine("Írj be egy eredményt." + "(Megfelelt/Megtagadva)");
                        vizsgalat.Add(Console.ReadLine());
                        Console.WriteLine("Írj be a Dátumot YYYY.MM.DD formátumba.");
                        vizsgalat.Add(Console.ReadLine());
                        this.logic.InsertVizsgalat(vizsgalat);
                        Console.WriteLine(this.logic.GetStringBuilderVizsgalat());
                    }
                    else if (category == "Edzo")
                    {
                        Console.WriteLine(this.logic.GetTableContentsEdzo());
                        List<string> edzo = new List<string>();
                        Console.WriteLine("Írj be egy ID." + "(Számot)");
                        edzo.Add(Console.ReadLine());
                        Console.WriteLine("Írj be Edzőnevét.");
                        edzo.Add(Console.ReadLine());
                        Console.WriteLine("Írj be egy klubot.");
                        edzo.Add(Console.ReadLine());
                        Console.WriteLine("Írj be egy AZ ovfokozatot.");
                        edzo.Add(Console.ReadLine());
                        Console.WriteLine("Írj be Elérhetőséget.");
                        edzo.Add(Console.ReadLine());
                        Console.WriteLine("Írj be egy Kort.");
                        edzo.Add(Console.ReadLine());
                        this.logic.InsertEdzo(edzo);
                        Console.WriteLine(this.logic.GetStringBuilderEdzo());
                    }
                    else if (category == "Orvos")
                    {
                        Console.WriteLine(this.logic.GetTableContentsOrvos());
                        List<string> orvos = new List<string>();
                        Console.WriteLine("Írj be egy ID." + "(Számot)");
                        orvos.Add(Console.ReadLine());
                        Console.WriteLine("Írj be egy Orvos nevet.");
                        orvos.Add(Console.ReadLine());
                        Console.WriteLine("Írj be egy szakirányt.");
                        orvos.Add(Console.ReadLine());
                        Console.WriteLine("Írj be egy  Elérhetőséget.");
                        orvos.Add(Console.ReadLine());
                        Console.WriteLine("Írj be mi a sportja.");
                        orvos.Add(Console.ReadLine());
                        Console.WriteLine("Írj be hogy vizsgalt-e már.");
                        orvos.Add(Console.ReadLine());
                        this.logic.InsertOrvos(orvos);
                        Console.WriteLine(this.logic.GetTableContentsOrvos());
                    }
                    else
                    {
                        Console.WriteLine("rossz táblanév");
                    }

                    Console.ReadLine();
                    Console.Clear();
                }
                else if (selectedMenuItem == "Törles")
                {
                    Console.Clear();
                    Console.WriteLine("Adjon meg egy táblát amiből törölni szeretne" + " (Versenyzo,Edzo,Orvos,Vizsgalat)");
                    string tablaNev = Console.ReadLine();
                    if (tablaNev == "Versenyzo")
                    {
                        Console.WriteLine(this.logic.GetStringBuilder());
                    }
                    else if (tablaNev == "Orvos")
                    {
                        Console.WriteLine(this.logic.GetTableContentsOrvos());
                    }
                    else if (tablaNev == "Vizsgalat")
                    {
                        Console.WriteLine(this.logic.GetStringBuilderVizsgalat());
                    }
                    else if (tablaNev == "Edzo")
                    {
                        Console.WriteLine(this.logic.GetTableContentsEdzo());
                    }
                    else
                    {
                        Console.WriteLine("Rossz táblanév");
                        Console.ReadLine();
                        Console.Clear();
                    }

                    Console.WriteLine("Adjon meg egy ID-t ami alapján törölni szeretne:");
                    int id = int.Parse(Console.ReadLine());
                    if (tablaNev == "Versenyzo")
                    {
                        if (this.logic.DeletVersenyzo2(id))
                        {
                            Console.WriteLine(this.logic.GetStringBuilder());
                        }
                        else
                        {
                            Console.WriteLine("Rossz ID" + " " + " Vagy " + "Az idegen kulcs miatt nem lehet törölni");
                        }
                    }
                    else if (tablaNev == "Orvos")
                    {
                        if (this.logic.DeletOrvos2(id))
                        {
                            Console.WriteLine(this.logic.GetTableContentsOrvos());
                        }
                        else
                        {
                            Console.WriteLine("Rossz ID" + " " + " Vagy " + "Az idegen kulcs miatt nem lehet törölni");
                        }
                    }
                    else if (tablaNev == "Vizsgalat")
                    {
                        if (this.logic.DeletVizsgalat2(id))
                        {
                            Console.WriteLine(this.logic.GetStringBuilderVizsgalat());
                        }
                        else
                        {
                            Console.WriteLine("Rossz ID" + " " + " Vagy " + "Az idegen kulcs miatt nem lehet törölni");
                        }
                    }
                    else if (tablaNev == "Edzo")
                    {
                        if (this.logic.DeletEdzo2(id))
                        {
                            Console.WriteLine(this.logic.GetStringBuilderEdzo());
                        }
                        else
                        {
                            Console.WriteLine("Rossz ID" + " " + " Vagy " + "Az idegen kulcs miatt nem lehet törölni");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Rossz id");
                    }

                    Console.ReadKey();
                    Console.Clear();
                }
                else if (selectedMenuItem == "Beillesztés")
                {
                    Console.Clear();
                    Console.WriteLine("Adjon meg egy tábla nevet  amibe beilleszteni  szeretne" + " (Versenyzo, Edzo, Orvos, Vizsgalat)");
                    string tablaNev = Console.ReadLine();
                    if (tablaNev == "Versenyzo")
                    {
                        Console.WriteLine(this.logic.GetStringBuilder());
                        List<string> versenyzo = new List<string>();
                        Console.Write("ID: ");
                        versenyzo.Add(Console.ReadLine());
                        Console.Write("Neve: ");
                        versenyzo.Add(Console.ReadLine());
                        Console.Write("Neme: ");
                        versenyzo.Add(Console.ReadLine());
                        Console.Write("Suly: ");
                        versenyzo.Add(Console.ReadLine());
                        Console.Write("Kor: ");
                        versenyzo.Add(Console.ReadLine());
                        Console.Write("Klub: ");
                        versenyzo.Add(Console.ReadLine());
                        Console.Write("Ovfokozat: ");
                        versenyzo.Add(Console.ReadLine());
                        Console.Write("Küzdlem: ");
                        versenyzo.Add(Console.ReadLine());
                        Console.Write("Formagyakorlat: ");
                        versenyzo.Add(Console.ReadLine());
                        Console.Write("Edzo_id: ");
                        versenyzo.Add(Console.ReadLine());
                        this.logic.UpdateVersenyzo(int.Parse(versenyzo[0]), versenyzo);
                        Console.WriteLine(this.logic.GetStringBuilder());
                        Console.ReadKey();
                    }
                    else if (tablaNev == "Orvos")
                    {
                        Console.WriteLine(this.logic.GetTableContentsOrvos());
                        List<string> orvos = new List<string>();
                        Console.Write("ID: ");
                        orvos.Add(Console.ReadLine());
                        Console.Write("Neve: ");
                        orvos.Add(Console.ReadLine());
                        Console.Write("Szakiranya: ");
                        orvos.Add(Console.ReadLine());
                        Console.Write("Elerhetosege: ");
                        orvos.Add(Console.ReadLine());
                        Console.Write("Sportja: ");
                        orvos.Add(Console.ReadLine());
                        Console.Write("Vizsgalt-e már: ");
                        orvos.Add(Console.ReadLine());
                        this.logic.UpdateOrvos(int.Parse(orvos[0]), orvos);
                        Console.WriteLine(this.logic.GetTableContentsOrvos());
                        Console.ReadKey();
                    }
                    else if (tablaNev == "Edzo")
                    {
                        Console.WriteLine(this.logic.GetTableContentsEdzo());
                        List<string> edzo = new List<string>();
                        Console.Write("ID: ");
                        edzo.Add(Console.ReadLine());
                        Console.Write("Neve: ");
                        edzo.Add(Console.ReadLine());
                        Console.Write("Klub: ");
                        edzo.Add(Console.ReadLine());
                        Console.Write("Elerhetosege: ");
                        edzo.Add(Console.ReadLine());
                        Console.Write("Ovfokozat: ");
                        edzo.Add(Console.ReadLine());
                        Console.Write("kor: ");
                        edzo.Add(Console.ReadLine());
                        this.logic.UpdateEdzo(int.Parse(edzo[0]), edzo);
                        Console.WriteLine(this.logic.GetTableContentsEdzo());
                        Console.ReadKey();
                    }
                    else if (tablaNev == "Vizsgalat")
                    {
                        Console.WriteLine(this.logic.GetStringBuilderVizsgalat());
                        List<string> viszgalat = new List<string>();
                        Console.Write("Viszgalat_ID: ");
                        viszgalat.Add(Console.ReadLine());
                        Console.Write("Versenyzo_ID: ");
                        viszgalat.Add(Console.ReadLine());
                        Console.Write("Orvos_ID: ");
                        viszgalat.Add(Console.ReadLine());
                        Console.Write("Eredmenye: ");
                        viszgalat.Add(Console.ReadLine());
                        Console.Write("Dátuma: ");
                        viszgalat.Add(Console.ReadLine());
                        this.logic.UpdateVizsgalat(int.Parse(viszgalat[0]), viszgalat);
                        Console.WriteLine(this.logic.GetStringBuilderVizsgalat());
                        Console.ReadKey();
                    }
                    else
                    {
                        Console.WriteLine("rossz tábla");
                    }
                }
                else if (selectedMenuItem == "5")
                {
                    Console.Clear();
                    Console.WriteLine("Edzők versenyzői: ");
                    Console.WriteLine("Írj be egy edző nevet:");
                    string nev = Console.ReadLine();
                    IQueryable q = this.logic.VersenyzoListazasaEdzoiNevekSzerint(nev);
                    if (q != null)
                    {
                        foreach (var item in q)
                        {
                            Console.WriteLine(item);
                        }
                    }
                    else
                    {
                        Console.WriteLine("Nincs Ilyen edző! ");
                    }

                    Console.ReadLine();
                    Console.Clear();
                }
                else if (selectedMenuItem == "6")
                {
                    Console.Clear();
                    Console.WriteLine("Listázzuk ki azokat az orvosokat,akik megtagadtak versenyzői engedélyt");
                    IQueryable q = this.logic.VersenyzoListOrvosListazasaMegtagadottVizsgalatiEredmenyAlapjanzasaEdzoiNevekSzerint();
                    foreach (var item in q)
                    {
                    Console.WriteLine(item);
                    }

                    Console.ReadLine();
                    Console.Clear();
                }
                else if (selectedMenuItem == "7")
                {
                    Console.Clear();
                    Console.WriteLine("A versenyen indulható versenyzők adatai: ");
                    IQueryable q = this.logic.VersenyzoEdzoje();
                    foreach (var item in q)
                    {
                        Console.WriteLine(item);
                    }

                    Console.ReadLine();
                    Console.Clear();
                }
                else if (selectedMenuItem == "Java Végpont")
                {
                    Console.Clear();
                    string s = this.logic.XmlFromURL();
                    this.logic.SplitXml(s);
                    Console.WriteLine("Download successful.");
                    Console.ReadLine();
                    Console.Clear();
                }
               else if (selectedMenuItem == "Kilépés")
                {
                    Console.Clear();
                    Console.WriteLine("Viszlát!");
                    Environment.Exit(0);
                }
            }
        }

        private static string DrawMenu(List<string> items)
        {
            for (int i = 0; i < items.Count; i++)
            {
                if (i == index)
                {
                    Console.ForegroundColor = ConsoleColor.DarkMagenta;

                    Console.WriteLine(items[i]);
                }
                else
                {
                    Console.WriteLine(items[i]);
                }

                Console.ResetColor();
            }

            ConsoleKeyInfo ckey = Console.ReadKey();

            if (ckey.Key == ConsoleKey.DownArrow)
            {
                if (index == items.Count - 1)
                {
                    index = 0;
                }
                else
                {
                    index++;
                }
            }
            else if (ckey.Key == ConsoleKey.UpArrow)
            {
                if (index <= 0)
                {
                    index = items.Count - 1;
                }
                else
                {
                    index--;
                }
            }
            else if (ckey.Key == ConsoleKey.Enter)
            {
                return items[index];
            }
            else
            {
                return string.Empty;
            }

            Console.Clear();
            return string.Empty;
}
    }
}
