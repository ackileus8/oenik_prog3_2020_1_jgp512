﻿create table Vizsgalat(
Vizsg_ID int PRIMARY KEY,
V_ID int REFERENCES Versenyzo(V_ID),
Orvos_ID int  REFERENCES Orvos(Orvos_ID),
Eredmenye VARCHAR(10) not null,
Datuma Date not null
);
