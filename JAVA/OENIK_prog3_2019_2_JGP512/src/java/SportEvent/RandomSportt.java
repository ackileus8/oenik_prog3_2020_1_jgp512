/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SportEvent;


import java.util.Date;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
/**
 *
 * @author nandi
 */
@XmlRootElement
public class RandomSportt {

    
    public Integer getVIzsgalat_ID() {
        return Vizsgalat_ID;
    }

    
    public Integer getVersenyzo_ID() {
        return Versenyzo_ID;
    }

    
    public Integer getOrvos_ID() {
        return Orvos_ID;
    }

    
    public String getEredmenye() {
        return Eredmenye;
    }

    
    public Date getNapja() {
        return Vizsgalat_napja;
    }

    public RandomSportt(int VIzsgalat_ID, int Versenyzo_ID, int Orvos_ID, String eredmenye, Date napja) {
        this.Vizsgalat_ID = VIzsgalat_ID;
        this.Versenyzo_ID = Versenyzo_ID;
        this.Orvos_ID = Orvos_ID;
        this.Eredmenye = eredmenye;
        this.Vizsgalat_napja = napja;
    }
    
   public RandomSportt(){
       
   }
    
    @XmlElement
    private  Integer Vizsgalat_ID;
  
    @XmlElement
    private  Integer Versenyzo_ID;
     
    @XmlElement
    private  Integer Orvos_ID;
     
    @XmlElement
    private  String Eredmenye;
     
    @XmlElement
    private  Date Vizsgalat_napja;
    
    
}
