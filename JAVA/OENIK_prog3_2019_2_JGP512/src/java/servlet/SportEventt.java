/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

import SportEvent.RandomSportt;
import java.io.IOException;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.IllegalFormatException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 *
 * @author nandi
 */



@WebServlet(name = "SportEvent", urlPatterns = {"/SportEvent"})
public class SportEventt extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    Random rnd = new Random();
    
    Integer vizsgalat_ID= rnd.nextInt(600);
    Integer versenyzo_ID= rnd.nextInt(500);
   
     Integer orvos_ID = rnd.nextInt(500) ;
    String eredmeny ;
    
//    Integer orvos_ID ;
//    String eredmeny = "megfellet";
//    
  Date datuma = new Date();
    
  
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
         
       try{
             //orvos_ID = Integer.parseInt(request.getParameter("orvos_id"));
              //  Orvos id-ja:<input type="number" name="orvos_id" min="1" max="500">
             eredmeny = request.getParameter("eredmeny");
            } catch(IllegalFormatException e){
                System.out.println(e.getMessage());
            }
       
            RandomSportt rp = new RandomSportt(vizsgalat_ID,versenyzo_ID,orvos_ID,eredmeny,datuma);
    
try{    
            JAXBContext context = JAXBContext.newInstance(RandomSportt.class);
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshaller.marshal(rp, response.getOutputStream());
        } catch(JAXBException ex){
            Logger.getLogger(SportEventt.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
